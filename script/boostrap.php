<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
$configs = require_once __DIR__.'/config.php';

$config =$configs['mysql'];
$connectStr = 'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8;port='.$config['port'];
$imsql = new PDO($connectStr,$config['user'],$config['password']);

function qs($query){
	global $isqlsv;
	$stmt = $isqlsv->query($query);
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function e($query){
	global $isqlsv;
	return $isqlsv->query($query);
}

function upsert($col,$data,$find=array()){
	$col->update($find,$data,array('upsert'=>true));
}

function insert($table,$params){
	$aps = array();
	$keys = array();
	foreach (array_keys($params) as $p){
	   $aps[] = ":$p";
	   $keys[] = "`{$p}`";
	}
	$aps = implode(',', $aps);
    $cps = implode(',',array_keys($params));
	$keys = implode(',',$keys);
	$buildQuery = "INSERT INTO $table ($keys) VALUES ($aps)\n";
	$aps = array();
	//update
	foreach (array_keys($params) as $p){
	   $aps[] = "`$p` = :$p";
	}
	$aps = implode(', ', $aps);
	$buildQuery .= " ON duplicate key UPDATE $aps;";
	return execute($buildQuery,$params);
}
function qs_mysql($query,$params=array()){
	global $imsql;
	$stmt = $imsql->query($query);
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function execute($query,$params=array()){
	global $imsql;
	$stmt = $imsql->prepare($query);
	foreach ($params as $param=>$value){
		if($value===''){
			$value = null;
		}
		$stmt->bindValue(':'.$param,$value);
	}
	if(!$result = $stmt->execute()){
		echo $stmt->errorInfo()[2];
		exit;
	}
	return $result;
}
function get_index($data,$key,$default=null){
    if(isset($data[$key])){
        return $data[$key];
    }
    return $default;
}
