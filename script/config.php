<?php 

return array(
    'mysql'=>array(
		'host'=>'localhost',
		'dbname'=>'tsthotel',
		'user'=>'root',
		'password'=>'',
		'port'=>3306,
		'log_file'=> API_PATH.'/logs/mysql_error.log',
    )
);
