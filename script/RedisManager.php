<?php
class RedisManager
{
	public static function setValue($key, $value, $time = 0) {
		try {
			//$iredis = Api::getService('redis');	
			global $iredis;
			$time = intval($time);
		
			if ($time > 0){
				$v_seted = $iredis->set($key, $value, $time); // time expire
			} else {
				$v_seted = $iredis->set($key, $value);
			}
			return $v_seted;
		} catch(\Exception $ex) {
			self::writeErrorLog($ex->getMessage());
		}
	}
	
	public static function getValue($key) {
		try {
			global $iredis;
			//$iredis = Api::getService('redis');	
			
			$v_value = $iredis->get($key);
			return $v_value;
		} catch(\Exception $ex) {
			self::writeErrorLog($ex->getMessage());
		}
	}
	
	public static function writeErrorLog($message){
        $content = date('Y-m-d H:i:s').' - ';
        $content .= $message."\n";
        file_put_contents(API_PATH.'/logs/redis_error.log', $content,FILE_APPEND);
    }
} 
?>