<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Email Order Template</title>
    </head>
    <body>
        <table style="font-family: Arial; border: 1px solid #dd4b39;width:710px;border-left: 1;border-right: 1;border-top: 1;border-bottom: 1;">
            <tr>
                <!-- Logo Snatchoo -->
                <td style="text-align: center" width="170px">
                    <font color="#dd4b39">
                        <a href="www.247post.vn">
                            <img src="http://247post.vn/uploads/images/banner/logo.png" style="max-width: 100%;" />
                        </a>
                    </font>
                </td>
                <td valign="top" style="font-size: 14pt;">
                    <span style="margin-left:60px"><strong> CÔNG TY CỔ PHẦN HAI BỐN  BẢY (247 SJC) </strong></span><br />
                    <span style="margin-left:140px">Speed is out advantage</span><br />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 10pt; font-family:Arial">
                    <br />
                    <br />
                    <p align="justify">
                        <h3>
                            Có đơn hàng đã được phân công đến bưu cục , vui lòng phân công nhân viên lấy hàng trong thời gian sớm nhất.<br />
                            <a href="<?php echo $url?>">Vào trang quản lý</a>
                        </h3>
                    </p>
                    <ul>
                        <?php foreach ($list as $val): ?>
                            <li>
                                Đơn hàng <strong><?php echo $val['OrderCustomerID']; ?></strong> từ <strong><?php echo $val['CustomerID']; ?></strong>.<br />
                                Địa chỉ nhận hàng : <?php echo $val['SenderAddress']; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center" style="font-size: 10pt; font-family:Arial; color: white" 
                    bgcolor="#dd4b39">
                    Lưu ý: Đây là email gửi tự động, vui long không gửi mail đến địa chỉ email này. <br />
                    Mọi thắc mắc và vấn đề về sử dụng phần mềm vui lòng gửi mail đến <a href="mailto:bugs@247post.vn">bugs@247post.vn</a>
                </td>
            </tr>
        </table>
    </body>
    <style>
        a img {max-width: 100%; }
    </style>
</html>