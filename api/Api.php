<?php
class Api{
	protected static $_routes = array();
    protected static $app = null;
    protected static $configs = array();
    protected static $services = array();

    public static function register($app){
        self::$app = $app;
    }
    
    public static function app(){
        return self::$app;
    }
    public static function log($message,$cat = 'Error'){
        $format_log = date('Y-m-d H:i:s').' - '.$message.' - '.$_SERVER['REQUEST_URI'].' - '.$_SERVER['REMOTE_ADDR']."\n";
        $log_path = self::getConfig('log');
        if(is_writable($log_path['error'])){
            file_put_contents($log_path['error'],$format_log,FILE_APPEND);
        }
    }

    public static function loadConfigs($configs = array()){
        self::$configs = $configs;
    }
    
    public static function getConfig($config,$default = null){
        return isset(self::$configs[$config])?self::$configs[$config]:$default;
    }
    
    public static function registerService($name,$service){
        self::$services[$name] = $service;
    }
    
    public static function getService($service,$default = null){
        if(isset(self::$services[$service])){
            if(self::$services[$service] && (self::$services[$service] instanceof Closure)){
                return call_user_func(self::$services[$service]);
            }
            else{
                return self::$services[$service];
            }
        }
        else{
            return $default;
        }
    }
}

class App{
    const DEFAULT_PORT=80;
    const DEFAULT_PORT_SSL=443;
    public static $routes = array();
    public $params = array();
    protected static $_routes = array();
    protected $_response_code = 200;
    protected static $_beforeDo = null;
    protected $_controller = null;
    public function getScheme(){
        if(isset($_SERVER['HTTPS'])&&strtolower($_SERVER['HTTPS'])=='on'){
			return 'https';
		}
		else{
			return 'http';
		}
    }

    public function getController(){
        return $this->_controller;
    }
    public function load($controller){
        $this->_controller = $controller;
        $classController = ucfirst($controller).'Controller';
        if(!file_exists('./controllers/'.$classController.'.php')){
            throw new Exception("Controller not found",404);
            exit;
        }
        include_once './controllers/'.$classController.'.php';
        if(!class_exists($classController)){
            throw new Exception("Controller not found ",404);
            exit;
            
        }
        return new $classController();
    }
    
    public function setResponseCode($response_code){
        $this->_response_code = $response_code;
    }


    public function responseCode(){
        return $this->_response_code;
    }
    public function response($result,$message='',$data=array(),$error=array()){
		ob_clean();
        header("HTTP/1.1 ".$this->responseCode());
        header('Content-Type: application/json');
        echo json_encode(array('result'=>$result,'message'=>$message,'data'=>$data,'error'=>$error));
        exit;
    }
    
    public function getBasePath(){
        $file_exe=basename(strtolower($_SERVER['SCRIPT_NAME']));
        return str_replace('/'.$file_exe,'',  strtolower($_SERVER['SCRIPT_NAME']));
    }
    public function getHost(){
        return !empty($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:NULL;
    }
    public function getPort(){
		return !empty($_SERVER['SERVER_PORT'])?$_SERVER['SERVER_PORT']:NULL;
	}
    
    public function getUrl(){
        return !empty($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:NULL;
    }
    public function getMethod(){
        return strtolower($_SERVER['REQUEST_METHOD']);
    }
    public function getBaseUrl(){
        static  $base_url=null;
        if($base_url==null){
            
            $base_url=$this->getSite().$this->getBasePath();
        }
        return $base_url;
    }
    public function getSite(){
        static $site=null;
        if($site==null){
            $site=$this->getScheme().'://'.$this->getHost();
            if($this->getPort()==self::DEFAULT_PORT||$this->getPort()==self::DEFAULT_PORT_SSL){
                $site=$this->getScheme().'://'.$this->getHost();
            }
        }
        return $site;
    }
    public function getIsPost(){
        if(!empty($_SERVER['REQUEST_METHOD'])&&  strtolower($_SERVER['REQUEST_METHOD']=='post')){
            return true;
        }
        else{
            return false;
        }
    }
    public function getIsAjax(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function getIsDelete(){
        if(!empty($_SERVER['REQUEST_METHOD'])&&  strtolower($_SERVER['REQUEST_METHOD']=='delete')){
            return true;
        }
        else{
            return false;
        }
    }
    public function getIsPut(){
        if(!empty($_SERVER['REQUEST_METHOD'])&&  strtolower($_SERVER['REQUEST_METHOD']=='put')){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function getPath(){
        return parse_url(trim(strtolower($_SERVER['REQUEST_URI'])),PHP_URL_PATH);
    }
    public function getPost($name,$default=NULL){
        return isset($_POST[$name])?$_POST[$name]:$default;
    }
    public function getPosts(){
        return (array)$_POST;
    }
    public function getParams(){
        return (array)$_GET;
    }
    public function getParam($name,$default=null){
        return isset($_GET[$name])?$_GET[$name]:$default;
    }
    public function getRealPath(){
        return trim(substr_replace($this->getPath(),'',0,  mb_strlen($this->getBasePath(),'utf-8')),'/');
    }
    
    public function getRequest(){
        return $this->getRealPath();
    }
    public function processRequest(){
        $request_params = \system\System::router()->parse($this->getRealPath());
        return $request_params;
    }
    
    protected function prepareSubPattern($r){
		$p = '/(?P<wp>\<(?P<sp>(\w)+):*(?P<pt>[^\>\/]*)\>)/i';
        $params = array();
        $pr = str_replace('/','\/',trim($r,' /'));
		if(preg_match_all($p,$r,$match)){
			$wps = $match['wp'];
			$sps = $match['sp'];
			$pts = $match['pt'];
			$nwps = array();
			foreach($wps as $key => $wp){
				if($pts[$key]===''){
					$pts[$key] = '[^\/]*';
				}
				$nwps[$wp] = "(?P<{$sps[$key]}>{$pts[$key]})";
			}
			$params = $sps;
			$new = str_replace($wps,$nwps,$pr);
			$new = ltrim($new,' /');
		}
        else{
            $new = ltrim($pr,' /');
            $params = array();
        }
        $new = '/^'.$new.'$/i';
		return array('pattern'=>$new,'param'=>$params);
	}
	public function parse($routes){
        $request = $this->getRequest();
        $callback = null;
		foreach ($routes as $_route){
			if($_route['method']!=null&&$_route['method']!=$this->getMethod()){
				continue;
			}
			$prepare = $this->prepareSubPattern($_route['route']);
			if(preg_match($prepare['pattern'],$request,$matches)){
				foreach ($matches as $key => $value) {
					if(in_array($key,$prepare['param'],true)){
						$this->params[$key] = $value;
					}
				}
                $callback = $_route['action'];
                break;
			}
		}
        return $callback;
	}
    
    
    public function run(){
        $callback = $this->parse(self::$_routes);
        if(empty($callback)){
            throw new HttpException(404,'Request not found');
        }
        $bf = true;
        if(!empty(self::$_beforeDo)){
            $bf = call_user_func_array(self::$_beforeDo,array());
        }
        if($bf){
            call_user_func_array($callback,$this->params);
        }
    }
    public function beforeDo($callback){
        self::$_beforeDo = $callback;
    }
    public function get($route,$action){
		static::$_routes[] = array('method'=>'get','route'=>$route,'action'=>$action);
	}
	public function post($route,$action){
		static::$_routes[] = array('method'=>'post','route'=>$route,'action'=>$action);
	}
	public function delete($route,$action){
		static::$_routes[] = array('method'=>'delete','route'=>$route,'action'=>$action);
	}
	public function put($route,$action){
		static::$_routes[] = array('method'=>'put','route'=>$route,'action'=>$action);
	}
	public function any($route,$action){
		static::$_routes[] = array('method'=>null,'route'=>$route,'action'=>$action);
	}
    
}

class HttpException extends Exception{
    public function __construct($code,$message = null,$previous=null) {
        parent::__construct($message, $code, $previous);
    }
}