<?php
//date_default_timezone_set('Asia/Ho_Chi_Minh');
ini_set('session.gc_maxlifetime',3600*24*100);
include_once "libs/vendor/autoload.php";
function autoload($class){
    $path = API_PATH.'/components/'.ucfirst($class).'.php';
    if(file_exists($path)){
        include_once $path;
        return true;
    }
    $path = API_PATH.'/models/'.ucfirst($class).'.php';
    if(file_exists($path)){
        include_once $path;
        return true;
    }
}
spl_autoload_register('autoload');
include_once API_PATH.'/Api.php';
include_once API_PATH.'/functions.php';
$configs = require_once API_PATH.'/config.php';
Api::loadConfigs($configs);
Api::register(new App());
Api::registerService('mysql',function () use ($configs) {
	static $imsql = null;
    if($imsql ==null){
        $config =$configs['mysql'];
        $connectStr = 'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8;port='.$config['port'];
        $imsql = new MysqlDb($connectStr,$config['user'],$config['password']);
        $imsql->setErrorLogFile($config['log_file']);
    }
    return $imsql;
});

Api::registerService('cache',function () use ($configs) {
    static $icache = null;
    if($icache ==null){
        $icache = new CacheData();
    }
    return $icache;
});