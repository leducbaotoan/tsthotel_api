<?php
if(defined('API_PATH') or define('API_PATH',__DIR__));
if(defined('API_DEBUG') OR define('API_DEBUG',true));
include_once API_PATH.'/bootrap.php';
include_once API_PATH.'/route.php';

set_error_handler(array(new ErrorHandler(API_DEBUG),'handle'));
Api::app()->beforeDo(function(){
	if($_SERVER['REQUEST_METHOD']=='POST'&&empty($_POST)){
		$_POST = json_decode(file_get_contents('php://input'), true);
	}
	header('Access-Control-Allow-Headers: Content-Type');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	header('Access-Control-Allow-Origin: *');
    return true;
});
try{
    Api::app()->run();
}
catch (Exception $ex){
	$ip = getenv('HTTP_CLIENT_IP')?:
	getenv('HTTP_X_FORWARDED_FOR')?:
	getenv('HTTP_X_FORWARDED')?:
	getenv('HTTP_FORWARDED_FOR')?:
	getenv('HTTP_FORWARDED')?:
	getenv('REMOTE_ADDR');
	if($ex->getCode()!='00'){
		@file_put_contents(API_PATH.'/logs/error_logs.log',
							date('Y-m-d H:i:s')." ".$ip." Exception ".$ex->getCode()." ".$ex->getMessage()." ".$_SERVER["REQUEST_URI"]." ".var_export($_REQUEST,true)."\n",FILE_APPEND);
	}
	if(API_DEBUG==true){
        if(get_class($ex)=='HttpException'){
            header("HTTP/1.1 ".$ex->getCode());;
        }
        else{
            header("HTTP/1.1 500");
        }
        echo "Exception ".$ex->getCode()." ".$ex->getMessage()."\n";
		echo $ex->getTraceAsString();
	}
    else{
        if(get_class($ex)=='HttpException'){
            Api::app()->setResponseCode($ex->getCode());
			Api::app()->response($ex->getCode(),$ex->getMessage());
        }
        else{
            Api::app()->setResponseCode(500);
			Api::app()->response(500,$ex->getMessage());
        }
    }
    Api::log("Exception ".$ex->getCode()." ".$ex->getMessage()." at line {$ex->getLine()} in {$ex->getFile()}");
    exit();
}