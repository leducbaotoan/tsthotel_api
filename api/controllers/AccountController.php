<?php 
class AccountController extends Controller{
	public function model(){
		return 'Account';
	}

	public function getUserDetails(){
		$model = new Account();
		$data = get_index($_POST,'userid');
		$result = $model->getUserDetails($data);
		$this->response();
	}

	public function doCreate(){
		$model = new Account();
		$data = get_index($_POST,'data');
		$result = $model->createUser($data);
		$this->result = $result['result'];
		$this->response();
	}

	public function doLogin(){
		$model = new Account();
		$user_name = get_index($_POST,'user_name');
		$password = get_index($_POST,'password');
		$pwnMD5 = md5($password);
		$result = $model->login($user_name,$pwnMD5);
		$this->data = $result['result'];
		$this->response();
	}
	
	public function doCheckMatchPassword() {
		$model 	= new Account();
		$userID 		= get_index($_POST, 'UserGroupID');
		$oldPassword 		= get_index($_POST, 'oldpassword');
		$result = $model->checkMatchPassword($userID, $oldPassword);
		$this->result = $result['result'];
		$this->message = $result['message'];
		$this->response();
	}

	public function doGetEmployeeByPostOffice($post_office){
		$this->data = $this->createModel()->getEmployeeByOffice($post_office);
		$this->response();
	}
	public function doLogout(){
		$token = get_index($_POST,'token');
		if(!empty($token)){
			session_start();
			session_id($token);
			session_destroy();
		}
		$this->response();
	}
	
	public function doCheckedLogin() {
		$tokenKey 		= get_index($_POST,'tokenKey');
		$tokenVal 		= get_index($_POST,'tokenVal');
		$auth 	= new Account();
		$result = $auth->checkedLogin($tokenKey, $tokenVal);
		if(!$result) {
			$this->result = -1;
			$this->message = 'Not login. Please login again';
			$this->data = array();
		} else {
			$this->result = 1;
			$this->message = 'Loging....';
			$this->data = array();
		}
		$this->response();
	}
}