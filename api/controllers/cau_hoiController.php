<?php
/**
 * Created by PhpStorm.
 * User: AnTony
 * Date: 11/29/2016
 * Time: 1:14 AM
 */
class cau_hoiController extends ApiController
{
    public function model()
    {
        return 'cau_hoi';
    }
    public function doCall(){
        $this->data = $this->createModel()->call();
        $this->response();
    }
    public function getListcauhoi()
    {
        $params = array();
        $params['NumberPage'] = get_index($_GET,'NumberPage',20);
        $params['NumberRow'] = get_index($_GET,'NumberRow',0);
        $this->data = $this->createModel()->CallListcauhoi($params);

        $this->response();

    }
    public function doInsertcauhoi(){
        $params = array(
            'ma_ch'       => get_index($_POST,'ma_ch',''),
            'noidung'       => get_index($_POST,'noidung',''),
            'lc1'       => get_index($_POST,'lc1',''),
            'lc2'       => get_index($_POST,'lc2',''),
            'lc3'       => get_index($_POST,'lc3',''),
            'lc4'       => get_index($_POST,'lc4',''),
            'dapan'       => get_index($_POST,'dapan',''),
            'made'       => get_index($_POST,'made',''),
            'diem'       => get_index($_POST,'diem',''),
            'traloi'       => get_index($_POST,'traloi',''),
            'trangthai'       => get_index($_POST,'trangthai',''),

        );
        $this->data = $this->createModel()->CallInsertcauhoi($params);
        $this->response();
    }

}