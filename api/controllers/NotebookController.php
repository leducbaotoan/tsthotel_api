<?php

/**
 * Created by PhpStorm.
 * User: THUVU <kenart1087@gmail.com>
 * Date: 26-11-2016
 * Time: 18:34
 */
class NotebookController extends ApiController
{
    public function model()
    {
        return 'NoteBook';
    }

    public function doIndex(){
        $model = $this->createModel();
        $this->data = $model->index($_GET);
        $this->response();
    }
}