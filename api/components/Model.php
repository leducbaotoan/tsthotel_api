﻿<?php
Abstract class Model {
    protected $_id = null;
    protected $_params = array();
    protected $_scenario = null;
    protected $_errors = array();
	
	public function setScenario($scenario){
        $this->_scenario = $scenario;
    }
    
    public function getScenario(){
        return $this->_scenario;
    }
    public function __construct($datas=array()) {
        $this->init();
        $this->setParams($datas);
    }
	protected function init(){
        foreach($this->params() as $param=>$value){
            if(!is_numeric($param)){
                $this->_params[$param] = $value;
            }
            else{
                $this->_params[$value] = null;
            }
        }
    }
	
	public function params(){
		return array();
	}
	
	public function validate(){
        $this->rules();
        if(!empty($this->_errors)){
            return false;
        }
        return true;
    }
	
	public function setParams($datas){
        foreach ($datas as $key=>$data){
            $this->setParam($key, $data);
        }
    }
    
    public function addError($param,$message){
        if(in_array($param,  array_keys($this->_params))){
            if(isset($this->_errors[$param])){
                $this->_errors[$param][] = $message;
            }
            else{
                $this->_errors[$param] = array($message);
            }
            return true;
        }
        return false;
    }
    
    public function validateRange($value,$min,$max){
        if(!$this->validateRequire($value)){
            return true;
        }
        if($min<=$value&&$value<=$max){
            return true;
        }
        return false;
    }
    public function validateRequire($value){
        if($value===null||$value===''){
            return false;
        }
        return true;
    }
    public function validateInt($value){
        if(!$this->validateRequire($value)){
            return true;
        }
        if(preg_match("/^-?[0-9]*$/D",$value)){
            return true;
        }
        return false;
    }
    
    public function validateMin($value,$min,$equal=false){
        if($equal==false){
            if($value>$min){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if($value>=$min){
                return true;
            }
            else{
                return false;
            }
        }
    }
    
    public function minRule($param,$min,$equal=false){
        $message = 'phải lớn hơn '.$min;
        if($equal===true){
            $message = 'phải lớn hơn hoặc bằng '.$min;
        }
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if($this->validateRequire($value)){
                    if(!$this->validateMin($value,$min,$equal)){
                        $this->addError($p, $message);
                    }
                }
            }
        }
        else{
            $value = $this->getParam($param);
            if($this->validateRequire($value)){
                if(!$this->validateMin($value,$min,$equal)){
                    $this->addError($param,$message);
                }
            }
        }
    }

    public function numberRule($param){
        $message = 'phải là kiểu số';
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if($this->validateRequire($value)){
                    if(!$this->validateNumber($value)){
                        $this->addError($p, $message);
                    }
                }
            }
        }
        else{
            $value = $this->getParam($param);
            if($this->validateRequire($value)){
                if(!$this->validateNumber($value)){
                    $this->addError($param,$message);
                }
            }
        }
    }
    
    public function validateEqual($value1,$value2,$type=false){
        if($type===true){
            return $value1===$value2?true:false;
        }
        else{
            return $value1==$value2?true:false;
        }
    }

	public function getParam($param,$default = null){
        return isset($this->_params[$param])?$this->_params[$param]:$default;
    }
	
	public function getParams(){
        return $this->_params;
    }
	public function setParam($param,$value){
        if(in_array($param,array_keys($this->_params))){
            $this->_params[$param] = $value;
            return true;
        }
        return false;
    }
    public function equalRule($param1,$param2,$type=false){
        $value1 = $this->getParam($param1);
        $value2 = $this->getParam($param2);
        $message = '{'.$param1.'} phải bằng {'.$param2.'}';
        if(!$this->equalRule($value1,$value2,$type)){
            $this->addError($param1,$value2);
        }
    }

    public function requiredRule($param){
        $message = 'không được rỗng';
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if(!$this->validateRequire($value)){
                    $this->addError($p, $message);
                }
            }
        }
        else{
            $value = $this->getParam($param);;
            if(!$this->validateRequire($value)){
                $this->addError($param, $message);
            }
        }
    }
    
    public function dateTimeRule($param,$format='Y-m-d H:i:s'){
        $message = 'định dạng phải là ngày giờ';
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if(!$this->validateDatetime($value,$format)){
                    $this->addError($p, $message);
                }
            }
        }
        else{
            $value = $this->getParam($param);
            if(!$this->validateDatetime($value,$format)){
                $this->addError($param, $message);
            }
        }
    }
    public function compareRule($param1,$param2,$type){
        $value1 = $this->getParam($param1);
        $value2 = $this->getParam($param2);
        if(!$this->validateRequire($value1)||!$this->validateRequire($value2)){
            return true;
        }
        $message = '';
        switch ($type){
            case 0:
                $message = ' phải bằng {'.$param2.'}';
                if(!$this->validateEqual($value1, $value2)){
                    $this->addError($param1,$message);
                }
            break;
            case 1:
                $message = ' phải lớn hơn {'.$param2.'}';
                if(!$this->validateMin($value1,$value2)){
                    $this->addError($param1,$message);
                }
            break;
            case 2:
                $message = ' phải lớn hơn hoặc bằng {'.$param2.'}';
                if(!$this->validateMin($value1,$value2,true)){
                    $this->addError($param1,$message);
                }
            break;
            case -1:
                $message = 'phải nhỏ hơn {'.$param2.'}';
                if(!$this->validateMax($value1,$value2)){
                    $this->addError($param1,$message);
                }
            break;
            case -2:
                $message = 'phải nhỏ hơn hoặc bằng {'.$param2.'}';
                if(!$this->validateMax($value1,$value2,true)){
                    $this->addError($param1,$message);
                }
            break;
        }
    }


    public function intRule($param){
        $message = 'phải là số nguyên';
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if($this->validateRequire($value)){
                    if(!$this->validateInt($value)){
                        $this->addError($p, $message);
                    }
                }
            }
        }
        else{
            $value = $this->getParam($param);
            if($this->validateRequire($value)){
                if(!$this->validateInt($value)){
                    $this->addError($param,$message);
                }
            }
        }
    }
    
    public function maxRule($param,$max,$equal=false){
        if($equal==false){
            $message = 'phải nhỏ hơn '.$max;
        }
        else{
            $message = 'phải nhỏ hơn hoặc bằng '.$max;
        }
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if($this->validateRequire($value)){
                    if(!$this->validateMax($value,$max,$equal)){
                        $this->addError($p, $message);
                    }
                }
            }
        }
        else{
            $value = $this->getParam($param);
            if($this->validateRequire($value)){
                if(!$this->validateMax($value,$max,$equal)){
                    $this->addError($param,$message);
                }
            }
        }
    }

    public function validateMax($value,$max,$equal=false){
        if($equal==false){
            if($value<$max){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            if($value<=$max){
                return true;
            }
            else{
                return false;
            }
        }
    }
   
    public function validateNumber($value){
        if(!$this->validateRequire($value)){
            return true;
        }
        return is_numeric($value);
    }
    
    public function addErrors($params,$message){
        foreach ($params as $param){
            $this->addError($param, $message);
        }
    }
}