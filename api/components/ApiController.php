<?php

abstract class ApiController implements IController{
    public $result = 0;
    public $message = 'Ok';
    public $errors = array();
    public $data = array();
	public $totalRows = 0;

    public function response() {
        ob_clean();
        header('Content-Type: application/json');
        echo json_encode(array('result'=>$this->result,
            'message'=>$this->message,
            'data'=>$this->data,
            'error'=>$this->errors,
            'totalRows'=>$this->totalRows));
        exit;
    }

    public function createModel($datas = array()){
        $classModel = $this->model();
        return new $classModel($datas);
    }
    
    public function doIndex(){
        $model = $this->createModel();
        $this->data = $model->index($limit,$offset,$sort,$order,$_GET);
        $this->response();
    }


    public function doGet($id) {
        $model = $this->createModel();
        if($data = $model->read($id)){
            $this->data = $data;
        }
        else{
            $this->result = -1;
            $this->message = 'Data not found';
        }
        $this->response();
    }

    public function doUpdate($id){
        $model = $this->createModel();
        $result = $model->update($id);
        $this->result = $result['result'];
        $this->message = $result['message'];
        $this->response();
    }

    public function doCreate() {
        $model = $this->createModel();
        $result = $model->save();
        $this->result = $result['result'];
        $this->message = $result['message'];
        $this->response();
    }
    public function doDelete($id) {
        $model = $model = $this->createModel();
        $result = $model->delete($id);
        $this->result = $result['result'];
        $this->message = $result['message'];
        $this->response();
    }
    
    public function doDeleteMulti(){
        $model = $model = $this->createModel();
        $result = $model->deleteMulti(get_index($_POST,'ids',array()));
        $this->result = $result['result'];
        $this->message = $result['message'];
        $this->response();
    }

    public function doUpdateMulti(){
        $model = $model = $this->createModel();
        $model->setScenario('updateMulti');
        $result = $model->updateMulti(get_index($_POST,'ids',array()),get_index($_POST,'data',array()));
        $this->result = $result['result'];
        $this->message = $result['message'];
        $this->response();
    }
}