<?php
class PushNotifications {
	const ANDROID 	= 'android';
	const IOS 		= 'ios';
	
	// (iOS) Private key's passphrase.
	private static $passphrase = 'joashp';
	
	// (Windows Phone 8) The name of our push channel.
    private static $channelName = "joashp";
	
	private static function useCurl($url, $headers, $fields = null) {
		// Open connection
        $ch = curl_init();
		
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }
     
            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
     
            // Close connection
            curl_close($ch);

            $finalRes = json_decode($result, true);
            return $finalRes;
		}
	}
	
	public static function pushAndroid($data, $reg_id, $module = '', $msgtype = '', $param = null) {
		$config = Api::getConfig('pushnotify');
		$url = 'https://android.googleapis.com/gcm/send';
        $message = array(
            'title' => $data['mtitle'],
            'message' => $data['mdesc'],
            'subtitle' => '',
            'tickerText' => '',
            'msgcnt' => 1,
            'vibrate' => 1,
            'module' => $module,
            'msgtype' => $msgtype,
            'param' => $param
        );
        
        $headers = array(
        	'Authorization: key=' .$config['android_key'],
        	'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'data' => $message,
        );
		$result = self::useCurl($url, $headers, json_encode($fields));
		if($result['success'] == 1) {
			$result['result'] = 1;
			$result['message'] = "Message successfully delivered";
		} else {
			$result['result'] = -1;
			$result['message'] = "Message not delivered";
		}
    	return $result;
	}
	
	public static function pushIOS($data, $devicetoken, $msgtype = '', $module = '', $param = null) {
		$config = Api::getConfig('pushnotify');
		$deviceToken = $devicetoken;
		$ctx = stream_context_create();

		// ck.pem is your certificate file
		stream_context_set_option($ctx, 'ssl', 'local_cert', $config['ios_key']);
		
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
			    'title' => $data['mtitle'],
                'body' => $data['mdesc'],
			 ),
			'sound' => 'default',
			'module' => $module,
            'msgtype' => $msgtype,
            'param' => $param
		);
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		// Close the connection to the server
		fclose($fp);
		if (!$result) {
			$data = array(
				'result'=>-1,
				'message'=>'Message not delivered'
			);
			return $data;
		} else {
			$data = array(
				'result'=>-1,
				'message'=>'Message successfully delivered'
			);
			return $data;
		}
	}
}
