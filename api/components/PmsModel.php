<?php

/**
 * Created by PhpStorm.
 * User: THUVU <kenart1087@gmail.com>
 * Date: 26-11-2016
 * Time: 18:25
 */
Abstract class PmsModel extends ApiModel{

    public function init(){
        parent::init();
    }

    public function prepareId($id){
        return trim(strtoupper($id));
    }

    public function setParams($params=array()){
        parent::setParams($params);
        $this->setParam($this->key(),$this->prepareId($this->getParam($this->key())));
    }

    public function beforeUpdate($id, $type = NULL){
        $this->setParam('LastEditDate',date('Y-m-d H:i:s'));
    }

}