<?php
class MysqlDb extends PDO {
    protected $_error_log = 'sql_errors.log';
    public function q($query,$params=array(),$fetchMode= PDO::FETCH_ASSOC){
        $stmt = $this->prepare($query);
        foreach ($params as $param=>$value){
            $stmt->bindValue(':'.$param, $value);
        }
        $stmt->execute();
        return $stmt->fetch($fetchMode);
    }
    
    public function setErrorLogFile($logFile){
        $this->_error_log = $logFile;
    }

    public function writeErrorLog($errorInfo,$query,$data = array()){
        $content = $errorInfo[0].'['.$errorInfo[1].'] - ';
        $content .= date('Y-m-d H:i:s').' - ';
        $content .= $query.' - ';
        $content .= $errorInfo[2].' - ';
        $content .= var_export($data,true)."\n";
        file_put_contents($this->_error_log,$content,FILE_APPEND);
    }
    public function qs($query,$params= array(),$fetchMode= PDO::FETCH_ASSOC){
        $stmt = $this->prepare($query);
        foreach ($params as $param=>$value){
            $stmt->bindValue(':'.$param,$value);
        }
        $result = $stmt->execute();
        if(!$result){
            $this->writeErrorLog($stmt->errorInfo(),$query,$params);
        }
        return $stmt->fetchAll($fetchMode);
    }
    public function e($query,$params=array()){
        $stmt = $this->prepare($query);
        foreach ($params as $param=>$value){
            if($value===''){
                $value = null;
            }
            $stmt->bindValue(':'.$param,$value);
        }
        $result = $stmt->execute();
        if(!$result){
            $this->writeErrorLog($stmt->errorInfo(),$query,$params);
        }
        return $result;
    }
    
    public function rollBack() {
        parent::rollBack();
    }

    protected function buildQueryForStoreProcedure($storeProcedure,$params){
        foreach($params as &$param){
            if($param===null){
                $param = '';
            }
        }
        $query='CALL '.$storeProcedure.'(';
        $arrPrams=array();
        foreach ($params as $key=>$value){
            $arrPrams[]=':'.$key;
        }
        $query=$query.implode(',', $arrPrams).')';
        return $query;
    }

    public function call($sp,$params = array(),$isClose=true){
        $query=  $this->buildQueryForStoreProcedure($sp, $params);
        $stmt = $this->prepare($query);
        foreach ($params as $param=>$value){
            if(is_array($value)){
				echo 'Mảng dữ liệu vào không đúng định dạng';
                var_dump($sp);
                var_dump($params);exit;
            }
            if($value===''){
                $value = null;
            }
            $stmt->bindValue(':'.$param,$value);
        }
        $result = $stmt->execute();
        if(!$result){
            $this->writeErrorLog($stmt->errorInfo(),$query,$params);
            return false;
        }
		if($isClose==true){
			$stmt->closeCursor();
		}
        return $stmt;
    }
	
    public function callFetch($sp,$params=array(),$fetchAll=0){
        $stmt = $this->call($sp,$params,false);
        if($stmt==false){
			//$stmt->closeCursor();
            return false;
        }
        if($fetchAll==0){
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($data===false){
				$data = array();
			}
			$stmt->closeCursor();
			return $data;
        }
        else{
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$stmt->closeCursor();
			if($data===false){
				$data = array();
			}
			return $data;
        }
    }

    public function callFetchMulti($sp,$params=array(),$fetchAll=0){
        $stmt = $this->call($sp,$params,false);
        if($stmt==false){
            return false;
        }
        $rs = array();
        do{
            if($fetchAll==0){
				$data = $stmt->fetch(PDO::FETCH_ASSOC);
				if($data===false){
					$data = array();
				}
                $rs[] = $data;
            }
            else{
				$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if($data===false){
					$data = array();
				}
                $rs[] = $data;
            }
        }while ($stmt->nextRowset());
		$stmt->closeCursor();
        return $rs;
    }
	
}