<?php

interface IController{
    public function response();
    public function model();
}

