<?php 
class CacheData implements ICache{
	public $time = 3600;
	public $path = './cache/';
	public function __construct($configs = array()){
		foreach($configs as $config=>$value){
			if(property_exists($this,$config)){
				$this->$config = $value;
			}
		}
	}	

	protected function hashKey($key){
		return md5($key);
	}

	protected function getCacheFile($key){
		return $this->path.$key;
	}

	public function get($key){
		$hash_key = $this->hashKey($key);
		$file_cache = $this->getCacheFile($hash_key);
		if(!file_exists($file_cache)){
		 	return false;
		}
		if(filemtime($file_cache) < time() - (int)$this->time){
			unlink($file_cache);
			return false;
		}
		return unserialize(file_get_contents($file_cache));
	}

	
	public function delete($key){
		$hash_key = $this->hashKey($key);
		$file_cache = $this->getCacheFile($hash_key);
		if(file_exists($file_cache)){
			@unlink($file_cache);
		}
	}
	public function set($key,$value){
		$hash_key = $this->hashKey($key);
		$file_cache = $this->getCacheFile($hash_key);
		$value = serialize($value);
		if(file_exists($file_cache)){
			if(filemtime($file_cache) > time() - (int)$this->time){
				return false;
			}
		}
		if(false !== ($f = @fopen($file_cache, 'w'))) {
	      fwrite($f,$value);	
	      fclose($f);
	    }
	    return true;
	}
}