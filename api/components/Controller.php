﻿<?php 
Abstract class Controller implements IController{
	public $result = 0;
    public $message = 'Ok';
    public $errors = array();
    public $data = array();
    public function __construct() {
        $this->init();
    }
    
    public function init(){
        
    }
    
    public function response(){
		Api::app()->response($this->result,$this->message,$this->data,$this->errors);
    }
    
    public function createModel($datas = array()){
        $classModel = $this->model();
        return new $classModel($datas);
    }
}