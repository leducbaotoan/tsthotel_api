<?php 
class ErrorHandler{
	protected $_debug;
	public function __construct($debug){
		$this->_debug = $debug;
		if($debug==true){
			error_reporting(E_ALL);
			ini_set('display_errors',1);
            ini_set('html_errors',1);
		}
		else{
			error_reporting(E_ERROR|E_USER_ERROR|E_PARSE);
			ini_set('display_errors',0);
		}
	}
	
	public function handle($errno,$errstr,$errfile,$errline,$errcontext=array()){
        ob_clean();
        $message = $this->getLevelError($errno).": $errstr at line $errline $errfile";
        if($this->_debug==false){
            Api::app()->setResponseCode(500);
            Api::app()->response(500,'Service error');
        }
        else{
            ob_start();
            header("HTTP/1.1 500");
            $data = ob_get_clean();
            echo $this->getLevelError($errno).": $errstr at line $errline $errfile\n";
            echo $data;
        }
        Api::log($message);
        exit;
	}
    
    public function getLevelError($level){
        switch ($level){
            case E_WARNING:
                return 'Warning';
            break;
            case E_USER_ERROR:
                return 'User error';
            break;
            case E_USER_WARNING:
                return 'User warning';
            break;
            case E_NOTICE:
                return 'Notice';
            break;
            case E_USER_NOTICE:
                return 'User notice';
            break;
            case E_PARSE:
                return 'Paser error';
            break;
            case E_WARNING:
                return 'Warning';
            break;
            default :
                return 'Error';
        }
    }
}