<?php
abstract class ApiModel {
    protected $_mysql = null;
    protected $_id = null;
    protected $_params = array();
    protected $_errors = array();

    public function __construct($datas=array()) {
        $this->init();
        $this->setParams($datas);
    }

    public function mysql(){
        if(empty($this->_mysql)){
            $this->_mysql = Api::getService('mysql');
        }
        return $this->_mysql;
    }

    public function key(){
        return '';
    }

    public function init(){
        foreach($this->params() as $param=>$value){
            if(!is_numeric($param)){
                $this->_params[$param] = $value;
            }
            else{
                $this->_params[$value] = null;
            }
        }
    }

    public function prepareId($id){
        return mb_strtoupper(trim($id));
    }

    public function setParams($datas){
        foreach ((array)$datas as $key=>$data){
            $this->setParam($key, $data);
        }
    }

    protected function getId(){
        return $this->_id;
    }

    protected function params(){
        return array();
    }

    protected function addParam($param,$value){
        $this->_params[$param] = $value;
    }

    public function getParam($param,$default = null){
        return isset($this->_params[$param])?$this->_params[$param]:$default;
    }

    public function checkParam($param){
        if(in_array($param,  array_keys($this->_params))){
            return true;
        }
        return false;
    }

    public function setParam($param,$value){
        if(in_array($param,array_keys($this->_params))){
            $this->_params[$param] = $value;
            return true;
        }
        return false;
    }
    public function getParams(){
        return $this->_params;
    }

    public function validate(){
        $this->rules();
        if(!empty($this->_errors)){
            return false;
        }
        return true;
    }

    public function validateParams($params,$callback,$message){
        $arrParams = array();
        foreach ($params as $param){
            if(call_user_func_array($callback,array($this->getParam($param)))==false){
                $arrParams[] = $param;
            }
        }
        $this->addErrors($arrParams,$message);
        return !empty($arrParams)?false:true;
    }

    public function addError($param,$message){
        if(in_array($param,  array_keys($this->_params))){
            if(isset($this->_errors[$param])){
                $this->_errors[$param][] = $message;
            }
            else{
                $this->_errors[$param] = array($message);
            }
            return true;
        }
        return false;
    }

    protected function rules(){

    }

    public function requiredRule($param){
        $message = 'không được rỗng';
        if(is_array($param)){
            foreach ($param as $p){
                $value = $this->getParam($p);
                if(!$this->validateRequire($value)){
                    $this->addError($p, $message);
                }
            }
        }
        else{
            $value = $this->getParam($param);;
            if(!$this->validateRequire($value)){
                $this->addError($param, $message);
            }
        }
    }

    public function validateDatetime($date,$format='Y-m-d H:i:s'){
        if(!$this->validateRequire($date)){
            return true;
        }
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function validateRequire($value){
        if($value===null||$value===''){
            return false;
        }
        return true;
    }

    public function getError($param){
        return isset($this->_errors[$param])?$this->_errors[$param]:array();
    }

    public function getErrors(){
        return $this->_errors;
    }

    public function beforeUpdate($id,$type=null){

    }
    public function beforeSave(){

    }
    public function beforeDelete($id){

    }
    public function afterUpdate($id,$type=null){

    }
    public function afterDelete($id){

    }
    public function afterSave($id){

    }

    public function prepareIndex($data){
        return $data;
    }

    public function conditionIndex($params = array()){
        return $params;
    }
    public function conditionRead($id){
        return array($this->key()=> $this->prepareId($id));
    }
    public function conditionUpdate($id){
        return array($this->key()=> $this->prepareId($id));
    }
    public function conditionDelete($id){
        return array($this->key()=> $this->prepareId($id));
    }

    public function read($id)
    {
        return array('result'=>0, 'data'=>array(), 'message'=>"");
	}
    public function index($params = array()){
        return array('result'=>0, 'data'=>array(), 'message'=>"");
    }

    public function save($id)
    {
        return array('result'=>0, 'data'=>array(), 'message'=>"");
    }

    public function delete($id)
    {
        return array('result'=>0, 'data'=>array(), 'message'=>"");
    }

    public function deleteMulti($id)
    {
        return array('result'=>0, 'data'=>array(), 'message'=>"");
    }
}