<?php
Api::app()->get('/', function () {
    echo "OK";
});

/**
 * DEFAULT API ROUTE
 * Please do not change
 */
Api::app()->get('/<section>/get/<id>',function($section,$id){
	$id = urldecode($id);
    Api::app()->load($section)->doGet($id);
});
Api::app()->post('/<section>/update/<id>',function($section,$id){
	$id = urldecode($id);
    Api::app()->load($section)->doUpdate($id);
});
Api::app()->post('/<section>/update-multi',function($section){
    Api::app()->load($section)->doUpdateMulti();
});
Api::app()->post('/<section>/delete/<id>',function($section,$id){
	$id = urldecode($id);
    Api::app()->load($section)->doDelete($id);
});
Api::app()->post('/<section>/delete-multi',function($section){
    Api::app()->load($section)->doDeleteMulti();
});
Api::app()->post('/<section>/create',function($section){
    Api::app()->load($section)->doCreate();
});
Api::app()->get('/<section>',function($section){
    Api::app()->load($section)->doIndex();
});

/**
 * CUSTOM API ROUTE
 */
Api::app()->post('/account/userdetails', function () {
    Api::app()->load('Account')->getUserDetails();
});
Api::app()->post('/account/login', function () {
    Api::app()->load('Account')->doLogin();
});
Api::app()->post('/account/checkmatchpassword',function() {
    Api::app()->load('Account')->doCheckMatchPassword();
});
Api::app()->post('/account/logout', function () {
    Api::app()->load('Account')->doLogout();
});

/*----- Cau hoi ---------*/
Api::app()->get('/cauhoi/getlist', function () {
    Api::app()->load('cau_hoi')->getListcauhoi();
});
Api::app()->post('/cauhoi/upsert', function () {
    Api::app()->load('cau_hoi')->doInsertcauhoi();
});




