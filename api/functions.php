<?php
function get_index($data,$key,$default=null){
    if(isset($data[$key])){
        return $data[$key];
    }
    return $default;
}

function sort_id($data,$key){
    usort($data, build_sorter($key));
}

function build_sorter($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($a[$key], $b[$key]);
    };
}
function find_data($data,$find=array()){
	$tmp = array();
	foreach($data as $value){
		$chk = true;
		foreach($find as $k=>$f){
			if($value[$k]!=$f){
				$chk = false;
				break;
			}
		}
		if($chk==true){
			$tmp[] = $value;
		}
	}
	return $tmp;
}
function sort_data($data,$field){
	usort($data,function($a,$b) use ($field){
		if($a[$field]==$b[$field]){
			return -1;
		}
		return $a[$field]<$b[$field]?1:-1;
	});
	return $data;
}
function prepare_text_search($text){
	if(mb_detect_encoding($text)=='UTF-8'){
		return $text;
	}
	$special_characters = array(
		'd'=>'đ',
		'D'=>'Đ'
	);
	$_arr_text = array();
	$arr_text = explode(' ', $text);
	foreach ($arr_text as $t) {
		$_arr_text[] = $t;
		$c_f = mb_substr($t,0,1);
		if(in_array($c_f,array_keys($special_characters))){
			$t_a = $special_characters[$c_f].mb_substr($t,1);
			$_arr_text[] = $t_a;
		}
	}
	return implode(' ', $_arr_text);
}

/**
 * Send message notify 
 * $message = array(
	'mtitle'=> "Title message",
	'mdesc'=> "Content message"
   );
 */
function sendMessageNotify($deviceType, $pushtoken, $mesage, $msgType, $module, $moduleParam) {
	if($deviceType == PushNotifications::ANDROID) {
		$rsPush = PushNotifications::pushAndroid($mesage, $pushtoken, $msgType, $module, $moduleParam);
	} else {
		$rsPush = PushNotifications::pushIOS($mesage, $pushtoken, $msgType, $module, $moduleParam);
	}
	return $rsPush;
}