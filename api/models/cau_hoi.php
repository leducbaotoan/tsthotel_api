<?php

/**
 * Created by PhpStorm.
 * User: AnTony
 * Date: 11/29/2016
 * Time: 1:14 AM
 */
class cau_hoi extends ApiModel
{
    public function collection()
    {
        return 'tbl_cauhoi';
    }
    public function Table()
    {
        return 'tbl_cauhoi';
    }
    public function CallListcauhoi($params)
    {
        $sp = 'tbl_cauhoi_procgetall';
        $data = $this->mysql()->callFetch($sp, $params, true);
        return $data;
    }
    public function CallInsertcauhoi($data)
    {
        $cauhoi = $data['cauhoi'];
        $is_update = isset($data['data']['is_update']) ? intval($data['data']['is_update']): 0;
        $result['is_update'] = $is_update;
        if($is_update == 0){
            $result['result'] = -1;
            $result['message'] = "Lỗi hệ thống(không xác định)";
            return $result;
        }

        try {
            $this->mysql()->beginTransaction();
            if ($is_update == 1) {//update
                //Update
                $sp = 'tbl_cauhoi_procUpdate';
                $rs = $this->mysql()->call($sp, $cauhoi);
                if($rs==false) {
                    $this->mysql()->rollBack();
                    $result['result'] = -1;
                    $result['message'] = "Lỗi hệ thống (update)";
                    return $result;
                }
            }if ($is_update == 2){
                //Insert
                $sp = 'tbl_cauhoi_procInsert';
                $rs = $this->mysql()->call($sp, $cauhoi);
                if($rs==false)
                    {
                    $this->mysql()->rollBack();
                    $result['result'] = -1;
                    $result['message'] = "Lỗi hệ thống(insert)";
                    return $result;
                }
                //End Insert
            }

            $result['result'] = 1;
            $result['message'] = "Cập nhật thành công";
            $this->mysql()->commit();
        } catch (Exception $ex) {
            //file_put_contents('aaads.log', $ex->getMessage());
            $this->mysql()->rollBack();
            $result['result'] = -1;
            $result['message'] = $ex->getMessage();
            return $result;
        }
        return $result;


    }


}