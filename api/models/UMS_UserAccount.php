<?php

class UMS_UserAccount extends PmsModel {
	const TYPE_ENCODE 	= 'sha1'; //sha512

	public function table() {
		return 'UMS_UserAccount';
	}

	public function prepareId($id){
        return strtoupper($id);
    }

	public function read($id,$params=array()){
        $result = array();
        return $result;
    }
	
	public function index($limit=10000, $offset = 0,$sort ='_id',
							$order='asc',$params = array()) {
		$arrReturns = array();
		$order = "desc";
        if($order=='asc'){
            $order = 1;
        } else {
            $order = -1;
        }

        $totalRows = 0;
        return array('datas'=>$arrReturns,'totalRows'=>$totalRows);
	}
}