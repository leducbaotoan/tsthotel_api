<?php

/**
 * Created by PhpStorm.
 * User: THUVU <kenart1087@gmail.com>
 * Date: 26-11-2016
 * Time: 18:00
 */
class NoteBook extends PmsModel
{
    public function table()
    {
        return 'TNS_Notebook';
    }

    public function conditionIndex($params = array())
    {
        return array(
            'pDocumentID' => get_index($params,'DocumentID')
        );
    }

    public function prepareId($id){
        return strtoupper($id);
    }

    public function read($id,$params=array()){
        $result = array();
        return $result;
    }

    public function index($params = array()) {
        $query = $this->conditionIndex($params);
        $data = $this->mysql()->callFetch('TNS_Notebook_procSelectAll',$query);
        return $data;
    }
}